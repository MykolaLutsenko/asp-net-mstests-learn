﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NinjectDILearn.Models;

namespace NinjectDILearn.Tests
{
    [TestClass]
    public class UnitTest2
    {
        private Product[] products = {
         new Product {Name = "Каяк", Category = "Водные виды спорта", Price = 275M},
         new Product {Name = "Спасательный жилет", Category = "Водные виды спорта", Price = 48.95M},
         new Product {Name = "Мяч", Category = "Футбол", Price = 19.50M},
         new Product {Name = "Угловой флажок", Category = "Футбол", Price = 34.95M}
         };


        [TestMethod]
        public void Sum_Products_Correctly()
        {
            // Arrange
            Mock<IDiscountHelper> mock = new Mock<IDiscountHelper>();
            mock.Setup(m => m.ApplyDiscount(It.IsAny<decimal>()))
                .Returns<decimal>(total => total);
            var target = new LinqValueCalculator(mock.Object);
            
            // Act
            var result = target.ValueProducts(products);

            // Assert
            Assert.AreEqual(products.Sum(e=>e.Price),result);
        }
    }
}
